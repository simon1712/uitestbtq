﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.UITest;
using Xamarin.UITest.Queries;


namespace UITestBTQ
{
    class LoginPageObject
    {
        public Func<AppQuery, AppQuery> EntryEmail = c => c.TextField("txtEmail");
        public Func<AppQuery, AppQuery> EntryPassword = c => c.TextField("txtPassword");
        public Func<AppQuery, AppQuery> ButtonLogin = c => c.Marked("btnLogin");

        IApp app;

        public LoginPageObject(IApp app)
        {
            this.app = app;
        }

        public void EnterEmail(String email)
        {
            app.EnterText(EntryEmail, email);
            app.DismissKeyboard();
        }

        public void EnterPassword(String password)
        {
            app.EnterText(EntryPassword, password);
            app.DismissKeyboard();
        }
        
        public void Login()
        {
            app.Tap(ButtonLogin);
        }
    }
}
