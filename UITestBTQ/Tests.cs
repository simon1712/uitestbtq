﻿using System;
using System.IO;
using System.Linq;
using NUnit.Framework;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace UITestBTQ
{
    [TestFixture(Platform.Android)]
   // [TestFixture(Platform.iOS)]
   // quynh xinh gai
    public class Tests
    {   
        IApp app;
        Platform platform;
        Func<AppQuery, AppQuery> EntryEmail = c => c.TextField("txtEmail");
        Func<AppQuery, AppQuery> EntryPassword = c => c.TextField("txtPassword");
        Func<AppQuery, AppQuery> ButtonLogin = c => c.Marked("btnLogin");
        Func<AppQuery, AppQuery> ButtonRegister = c => c.Marked("btnRegister");

        LoginPageObject loginPage;
        MainPageObject mainPage;

        public Tests(Platform platform)
        {
            this.platform = platform;
        }

        [SetUp]
        public void BeforeEachTest()
        {
            app = AppInitializer.StartApp(platform);
            loginPage = new LoginPageObject(app);
            mainPage = new MainPageObject(app);

        }

        [Test]
        public void LoginCompleted()
        {
            app.EnterText(EntryEmail, "quynhanh@gmail.com");
            app.DismissKeyboard();
            app.EnterText(EntryPassword, "17121995");
            app.Tap(ButtonLogin);
            //app.EnterText(c => c.Marked("txtPassword"), "17121995");
            //app.DismissKeyboard();
            //app.Tap(c => c.Marked("btnLogin"));
            
            app.Repl();
        }

        [Test] 
        
        public void LoginError()
        {
            app.EnterText(c => c.Marked("txtEmail"), "quynhanh55@gmail.com");
            app.DismissKeyboard();
            app.EnterText(c => c.Marked("txtPassword"), "17121995");
            app.DismissKeyboard();
            app.Tap(c => c.Marked("btnLogin"));
            app.WaitForElement(c => c.Text("Login Error"), timeout: TimeSpan.FromSeconds(5));
            AppResult[] result = app.Query("Login Error");
            Assert.IsTrue(result.Any(), "Login Failed");
            //app.Repl();
        }
        [Test]
        public void testOrder()
        {
            loginPage.EnterEmail("quynhanh@gmail.com");
            app.Screenshot("Enter input email");

            loginPage.EnterPassword("17121995");
            app.Screenshot("Enter input password");

            loginPage.Login();
            app.Screenshot("Click button sign in");

            app.Screenshot("Main Page");
            app.WaitForElement(mainPage.GetMainListItemAt(0), timeout: TimeSpan.FromSeconds(15));
            app.Tap(mainPage.GetMainListItemAt(0));

            app.Screenshot("Order Page");
            app.Tap(c => c.Text("Main"));
            //app.Repl();

            

            app.Screenshot("Select Chicken");
            app.Tap(c => c.Marked("NoResourceEntry-82").Child(1));
            //app.Tap(c => c.Text("$ 4"));

            app.Screenshot("Click cart ");
            app.Tap(c => c.Marked("NoResourceEntry-81"));

            app.Screenshot("Confirm Order");
            app.Tap(c => c.Text("Place The Order"));

            
            app.Tap(c => c.Text("Complete Order"));

            app.Screenshot("Payment");
            app.Tap(c => c.Marked("Complete Payment"));

            app.Screenshot("Status");
            app.Tap(c => c.Text("New Order"));
            app.WaitForElement(mainPage.GetMainListItemAt(0), timeout: TimeSpan.FromSeconds(10));

            //app.Repl();

        }
    }
}

