﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace UITestBTQ
{
    class MainPageObject
    {
        public Func<AppQuery, AppQuery> MerchantList = c => c.Marked("lstMain");
        public Func<AppQuery, AppQuery> EntryKeyword = c => c.TextField("txtKeyword");
        public Func<AppQuery, AppQuery> ButtonCurrentLocation = c => c.Marked("btnCurrentLocation");
        public Func<AppQuery, AppQuery> EntryTittle = c => c.TextField("txtTittle");
        public Func<AppQuery, AppQuery> EntryAddress = c => c.TextField("txtAddress");
        public Func<AppQuery, AppQuery> EntryDuration = c => c.TextField("txtDuration");
        public Func<AppQuery, AppQuery> ButtonDetail = c => c.Marked("btnDetail");
        public Func<AppQuery, AppQuery> ButtonHot = c => c.Marked("btnHot");
        public Func<AppQuery, AppQuery> ButtonWallet = c => c.Marked("btnWallet");
        public Func<AppQuery, AppQuery> ButtonMap = c => c.Marked("btnMap");
        public Func<AppQuery, AppQuery> ButtonNotification = c => c.Marked("btnNotification");


        IApp app;

        public MainPageObject(IApp app)
        {
            this.app = app;
        }

        public void EnterKeyword(string keyword)
        {
            app.EnterText(EntryKeyword, keyword);
            app.DismissKeyboard();
        }

        public Func<AppQuery, AppQuery> GetMainListItemAt(int index)
        {
            return c => c.Marked("lstMain").Child(index).Child(0).Child(0).Child(4);
        }

        public void Hot()
        {
            app.Tap(ButtonHot);
        }
        public void Wallet()
        {
            app.Tap(ButtonWallet);
        }
        public void Map()
        {
            app.Tap(ButtonMap);
        }
        public void Notification()
        {
            app.Tap(ButtonNotification);
        }
    }
}
